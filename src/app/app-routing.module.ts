import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginPageComponent } from '@components/login-page/login-page.component';
import { HomePageComponent } from '@components/home-page/home-page.component';
import { AuthGuard } from '@guards/auth.guard';
import { ReesterPageComponent } from '@components/reester-page/reester-page.component';
import { Grid01Component } from '@components/grid01/grid01.component';
import { Grid02Component } from '@components/grid02/grid02.component';

const routes: Routes = [
  { path: '', component: LoginPageComponent },
  { path: 'Operations', component: HomePageComponent, canActivate: [AuthGuard], children: [
    { path: '', component: Grid02Component },
    { path: 'GridOperations01', component: Grid01Component },
    { path: 'GridOperations02', component: Grid02Component }
  ] },
  { path: 'Reester', component: ReesterPageComponent, canActivate: [AuthGuard], children: [
    { path: 'Grid01', component: Grid01Component },
    { path: 'Grid02', component: Grid02Component }
  ] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
