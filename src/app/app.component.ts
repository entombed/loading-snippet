import { Component, HostListener } from '@angular/core';
import { environment } from '@environments/environment';
import { TranslateService } from '@ngx-translate/core';
import { UserSettingsService } from '@share/services/user-settings.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {

  public showContextMenu = false;
  private targetContextMenu: any;
  private isDevelopmentMode: boolean; // работаем в режиме разработки или продакшен
  public contextMenu = [
    {
      label: 'Get Component Name',
      command: () => { this.showNameOfApp(); }
    }
  ];

  constructor(
    private translateService: TranslateService,
    private userSettingsService: UserSettingsService,
  ) {
    this.userSettingsService.setLang('ru'); // сохраняем в сервисе язык приложения
    const lang = this.userSettingsService.getLang(); // забираем язык из сервиса
    this.translateService.use(lang); // говорим какой язык использовать для переводов
    this.isDevelopmentMode = environment.development; // проверяем в каком режиме работаем
  }

  ngOninit() {
  }


  /**
   * отлавливаем нажатие Ctrl
   * разрешаем вызывать свое контестное меню только при условии:
   *    работаем в режиме разработки
   *    нажат Ctrl
   *
   * @author A.Bondarenko
   * @date 2020-01-14
   * @param {*} event
   * @memberof AppComponent
   */
  @HostListener('document:keydown.Control', ['$event'])
  onCtrlPress(event: KeyboardEvent) {
    if (this.isDevelopmentMode) {
      this.showContextMenu = true;
    }
  }

  /**
   * захватываем DOM елемент на котором вызвали контекстное меню
   * при условии что работаем в режиме разработки
   *
   * @author A.Bondarenko
   * @date 2020-01-14
   * @param {MouseEvent} event
   * @memberof AppComponent
   */
  @HostListener('window:contextmenu', ['$event'])
  onContextMenu(event: MouseEvent): void {
    if (this.isDevelopmentMode) {
      this.targetContextMenu = event.target;
    }
  }

  /**
   * выводим в консоль имя родительского angular компонента
   * на котором вызвали контекстное меню
   *
   * @author A.Bondarenko
   * @date 2020-01-14
   * @private
   * @memberof AppComponent
   */
  private showNameOfApp() {
    let domElement: HTMLElement = this.targetContextMenu;
    let i = 0;
    while (domElement && ('parentElement' in domElement)) {
      const localName = domElement.localName.toLowerCase();
      if (/^app-\w+/i.test(localName)) {
        console.log(localName);
        break;
      }
      domElement = domElement.parentElement;
      i++;
    }
  }
}
