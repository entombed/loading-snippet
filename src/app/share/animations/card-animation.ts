import { trigger, style, transition, animate, keyframes, query, stagger, state } from '@angular/animations';

export const AnimatiModalCard =
  trigger('modalCard', [
    // state('close', style({
    //   left: '-100%'
    // })),
    transition(':enter', [
      animate(300, keyframes([
        style({ opacity: .5, offset: 0, left: '-100%' }),
        style({ opacity: 1, offset: 1.0, left: '0%' }),
      ]))
    ]),
    transition('visible => close', [
      animate(300, keyframes([
        style({ opacity: .9, offset: 1.0, left: '100%' }),
      ]))
    ])
  ]);
