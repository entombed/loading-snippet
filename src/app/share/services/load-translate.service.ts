// import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

// @Injectable({
//   providedIn: 'root'
// })
export class LoadTranslateService {

  private lang = 'ru';
  protected translationDictionary: object = {};

  constructor(
    protected translateService: TranslateService,
  ) { }

  protected setLanguage(lang = this.lang) {
    this.lang = lang;
    this.translateService.use(this.lang);
  }

  protected getLanguage() {
    return this.lang;
  }

  /**
   *
   *
   * @author A.Bondarenko
   * @date 2020-01-27
   * @protected
   * @param {string[]} [i18nWords] - массив слов которые необходимо перевести
   * @param {(() => void)} [callBack] - callback функция
   * @memberof BaseCardComponent
   */
  protected getTranslates(i18nWords: string[], translateService: TranslateService, lang, callBack?: (() => void)) {
    translateService.use(lang);
    let translationDictionary;
    translateService.get(i18nWords).subscribe((translatedWords) => {
      translationDictionary = translatedWords;
      if (callBack) {
        callBack();
      }
    });
    return translationDictionary;
  }


}
