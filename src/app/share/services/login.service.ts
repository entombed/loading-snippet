import { Injectable } from '@angular/core';
import { LoadingSnippetService } from '@modules/snippet/loading-snippet.service';
import { HolidayBannerService, HolidayClasses } from '@modules/holiday-banner/holiday-banner.service';
import { UserSettingsService } from './user-settings.service';


@Injectable({
  providedIn: 'root'
})
export class LoginService {
  // private loggedIn = false;
  constructor(
    private loadingSnippetService: LoadingSnippetService,
    private holidayBannerService: HolidayBannerService,
    private userSettingsService: UserSettingsService,
  ) { }

  public isAuthenticated() {
    this.loadingSnippetService.show(); // запускаем отображение рагрузки
    return new Promise(
      (resolve, reject) => {
        setTimeout(
          () => {
            this.holidayBannerService.setHolidayName('new-year-banner'); // указываем какой баннер отображать
            this.loadingSnippetService.hide(); // прячем отображение рагрузки
            this.userSettingsService.setIsLoggIn(true);
            resolve(this.userSettingsService.getIsLoggIn());
          },
          1000
        );
      }
    );
  }

  public login() {
    // this.loggedIn = true;
  }

  public logout() {
    // this.loggedIn = false;
    this.userSettingsService.setIsLoggIn(false);
  }

  // public randomInt() {
  //   const rand = 1 + Math.random() * (100 + 1 - 1);
  //   console.log(rand);
  //   console.log(Math.floor(rand) % 2 === 0);
  //   if (Math.floor(rand) % 2 === 0) {
  //     this.login();
  //   } else {
  //     this.logout();
  //   }
  // }
}
