import { TranslateService } from '@ngx-translate/core';
export class Translate {

  // перевод объекта
  public static async getTranslateObj(obj: any, translateService: TranslateService, callback?: (() => void)) {
    const promises = [];
    Object.keys(obj).forEach((key) => {
      const newPr = new Promise(function (resolve, reject) {
        translateService.get(key).subscribe((res: string) => {
          obj[key] = res;
          resolve();
        });
      });
      promises.push(newPr);
    });
    Promise.all(promises).then(function (values) {
      if (callback) {
          callback();
      }
    });
  }

}
