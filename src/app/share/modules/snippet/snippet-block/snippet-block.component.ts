import { Component, OnInit, ViewChild, Renderer2, ElementRef } from '@angular/core';
import { LoadingSnippetService } from '../loading-snippet.service';

@Component({
  selector: 'app-snippet-block',
  templateUrl: './snippet-block.component.html',
  styleUrls: ['./snippet-block.component.less']
})
export class SnippetBlockComponent implements OnInit {

  @ViewChild('snippetRef', { static: false }) snippetRef: ElementRef; // ссылка на html елемент сниппета загрузки

  constructor(
    private renderer: Renderer2,
    private loadingSnippetService: LoadingSnippetService,
  ) {
    this.listeningLoadingEvent();
  }

  ngOnInit() {
  }

  /**
   * Подписываемся на событие которое сохраняемся в loadingSnippetService
   * (сервис дергается в интерсепторе)
   * если пришло true отображаем сниппер
   * если пришло false прячем сниппер
   *
   * @author A.Bondarenko
   * @date 2020-01-02
   * @private
   * @memberof ProgressSpinnerComponent
  */
  private listeningLoadingEvent() {
    this.loadingSnippetService.isLoading.subscribe((status: boolean) => {
      if (this.snippetRef && status) {
        this.renderer.removeClass(this.snippetRef.nativeElement, 'hide');
      } else if (this.snippetRef && !status) {
        this.renderer.addClass(this.snippetRef.nativeElement, 'hide');
      }
    });
  }

}
