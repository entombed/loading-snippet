import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoadingSnippetService {

  public isLoading: Subject<boolean> = new Subject<boolean>();
  constructor() { }

  /**
   * закидываем в Subject значение true
   * для отображения сниппета
   *
   * @author A.Bondarenko
   * @date 2020-01-02
   * @memberof LoadingSnippetService
   */
  public show() {
    this.isLoading.next(true);
  }

  /**
   * закидываем в Subject значение false
   * для срытия сниппета
   *
   * @author A.Bondarenko
   * @date 2020-01-02
   * @memberof LoadingSnippetService
   */
  public hide() {
    this.isLoading.next(false);
  }
}
