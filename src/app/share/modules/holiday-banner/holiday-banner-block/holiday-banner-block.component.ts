import { Component, OnInit } from '@angular/core';
import { HolidayBannerService, HolidayClasses } from '../holiday-banner.service';

@Component({
  selector: 'app-holiday-banner-block',
  templateUrl: './holiday-banner-block.component.html',
  styleUrls: ['./holiday-banner-block.component.less']
})


export class HolidayBannerBlockComponent implements OnInit {
  public holidayAdditionalClass: HolidayClasses;

  constructor(
    private holidayBannerService: HolidayBannerService,
  ) {
    this.holidayAdditionalClass = this.holidayBannerService.getHolidayName();
  }

  ngOnInit() {
  }

}
