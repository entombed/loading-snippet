import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HolidayBannerBlockComponent } from './holiday-banner-block/holiday-banner-block.component';
import { HolidayBannerService } from './holiday-banner.service';


@NgModule({
  declarations: [HolidayBannerBlockComponent],
  imports: [
    CommonModule
  ],
  exports: [
    HolidayBannerBlockComponent
  ],
  providers: [
    HolidayBannerService
  ]
})
export class HolidayBannerModule { }
