import { Component, OnInit, Renderer2, ElementRef } from '@angular/core';
import { MessageService } from 'primeng/api';

export class BaseGridComponent implements OnInit {

  public cols: object[] = [];
  public dataGrid: object[] = [];
  public first = 0;
  public isScrollableGrid = true;
  public scrollHeightGrid: string = null;
  public minScrollHeightGrid = '100px';
  public maxScrollHeightGrid = '800px';
  public selectedCards: any[] = null;

  constructor(
    protected elementRef: ElementRef,
    protected renderer: Renderer2,
    protected messageService: MessageService,
  ) {
    this.scrollHeightGrid = this.minScrollHeightGrid;
  }

  ngOnInit() {
    // console.log(this.elementRef)
  }

  /**
   * ловим события по нажатию кнопки свернуть\развернуть блок
   *
   * @author A.Bondarenko
   * @date 2020-01-16
   * @param {*} event
   * @memberof BaseGridComponent
   */
  public clickToggleButton(event) {
    this.shangeSizeContent(event.originalEvent.target, event.checked);
  }

  public rowSelect(event) { }

  /**
   * ищем необходимые DOM элементы для разворачивания или сворачивания блока
   *
   * @author A.Bondarenko
   * @date 2020-01-16
   * @private
   * @param {HTMLElement} event DOM элемент в которо произошло нажатие
   * @param {boolean} checked true - развернуть блок, false - свернуть блок
   * @memberof BaseGridComponent
   */
  private shangeSizeContent(event: HTMLElement, checked: boolean) {
    const domElement = event;
    const bodyElement = this.renderer.selectRootElement('body', true);
    const tableBlockWrapper = this.searchWrapperBlock(domElement, 'be-table-block-wrapper');
    // const tableScrollableBody = this.searchDeepElement(tableBlockWrapper, '.ui-table-scrollable-body');
    if (checked) {
      // this.renderer.setStyle(tableScrollableBody, 'height', '700px');
      this.setExpandBlock(bodyElement, tableBlockWrapper);
    } else {
      // this.renderer.removeStyle(tableScrollableBody, 'height');
      this.setCollapseBlock(bodyElement, tableBlockWrapper);
    }
  }

  /**
   * ищем элемент обертку блока по классу,
   * который хотим развернуть на весь экран
   *
   * @author A.Bondarenko
   * @date 2020-01-16
   * @private
   * @param {HTMLElement} domElement DOM элемент внутри которого произошло событие
   * @param {string} wrapperClass имя css класса для обетки
   * @returns {HTMLElement}
   * @memberof BaseGridComponent
   */
  private searchWrapperBlock(domElement: HTMLElement, wrapperClass: string): HTMLElement {
    while (domElement && ('parentElement' in domElement)) {
      const className = domElement.className.split(' ');
      if (className.includes(wrapperClass)) {
        break;
      }
      domElement = domElement.parentElement;
    }
    return domElement;
  }

  private searchDeepElement(domElement: HTMLElement, className: string): Element {
    return domElement.querySelector(className);
  }

  /**
   * разворачиваем блок на весь экран
   *
   * @author A.Bondarenko
   * @date 2020-01-16
   * @private
   * @param {HTMLElement} bodyElement элемент body
   * @param {HTMLElement} domElement DOM элемент обертка нашего блока
   * @memberof BaseGridComponent
   */
  private setExpandBlock(bodyElement: HTMLElement, domElement: HTMLElement): void {
    this.renderer.setStyle(bodyElement, 'overflow-y', 'hidden');
    this.renderer.addClass(domElement, 'be-full-screen-wrapper');
    this.scrollHeightGrid = this.maxScrollHeightGrid;
  }
  /**
   * сворачиваем блок
   * возвращаем его на свое положение
   *
   * @author A.Bondarenko
   * @date 2020-01-16
   * @private
   * @param {HTMLElement} bodyElement
   * @param {HTMLElement} domElement
   * @memberof BaseGridComponent
   */
  private setCollapseBlock(bodyElement: HTMLElement, domElement: HTMLElement): void {
    this.renderer.removeStyle(bodyElement, 'overflow-y');
    this.renderer.removeClass(domElement, 'be-full-screen-wrapper');
    this.scrollHeightGrid = this.minScrollHeightGrid;
  }

  protected showInfoMsg(message: string): void {
    this.messageService.add(
      { severity: 'info', summary: 'Information Message', detail: message }
    );
  }
  protected showErrorMsg(message: string): void {
    this.messageService.add(
      { severity: 'error', summary: 'Something went wrong', detail: message, life: 3000 }
    );
  }

  public handleRowDblclick() {}

}
