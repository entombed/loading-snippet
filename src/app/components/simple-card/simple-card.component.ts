import { Component, OnInit, HostListener, Output, EventEmitter, ElementRef, Renderer2, AfterViewInit, Input } from '@angular/core';
import { MessageService } from 'primeng/api';
import { BaseCardComponent } from '@share/base-components/base-card/base-card.component';
import { AnimatiModalCard } from '@share/animations/card-animation';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-simple-card',
  templateUrl: './simple-card.component.html',
  styleUrls: ['./simple-card.component.less'],
  animations: [AnimatiModalCard]
})
export class SimpleCardComponent extends BaseCardComponent implements OnInit, AfterViewInit {

  @Output() closeCard: EventEmitter<any> = new EventEmitter<any>();

  cities = [
    { label: 'New York New York New York', value: 'NY' },
    { label: 'Rome', value: 'RM' },
    { label: 'London', value: 'LDN' },
    { label: 'IstanbulIstanbulIstanbulIstanbul 123London London ', value: 'IST' },
    { label: 'Paris', value: 'PRS' }
  ];
  val1 = 'Option 1';
  val2 = 'Option 22';
  date8 = new Date();
  selectedCategories: string[] = ['Technology', 'Sports'];
  selectedItem: any = null;
  currentIndex: any = null;
  showNewWindow: boolean;
  // bodyElement: ElementRef;

  // flags = {
  //   editingModeOff: true,
  // };
  i18nWords = ['SAVED_MSG', 'CLOSE_MSG'];
  displayDialogInfo: boolean;

  constructor(
    protected elementRef: ElementRef,
    protected renderer: Renderer2,
    protected messageService: MessageService,
    protected translateService: TranslateService,
  ) {
    super(elementRef, renderer, messageService, translateService);
  }

  ngOnInit() {
    this.getTranslates(this.i18nWords);
  }

  public focusOut(event) {
    console.log(event)
  }

  public dropDown(event) {
    console.log(event)
  }
  public openTab(event) {
    console.log(event)
  }

  openNewWindow() {
    this.showNewWindow = true;
  }
  saveCard() {
    super.saveCard()
    this.displayDialogInfo = true;
  }

}
