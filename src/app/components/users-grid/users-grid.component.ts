import { Component, OnInit, Renderer2, ElementRef } from '@angular/core';
import { MessageService } from 'primeng/api';
import { HttpApiService } from '@share/services/http-api.service';
import { BaseGridComponent } from '@share/base-components/base-grid/base-grid.component';

@Component({
  selector: 'app-users-grid',
  templateUrl: './users-grid.component.html',
  styleUrls: ['./users-grid.component.less']
})
export class UsersGridComponent extends BaseGridComponent implements OnInit {
  showSimpleCard: boolean;
  constructor(
    protected messageService: MessageService,
    protected httpApiService: HttpApiService,
    protected renderer: Renderer2,
    protected elementRef: ElementRef
  ) {
    super(elementRef, renderer, messageService);
  }

  ngOnInit() {
    this.createCols();
    this.getData();
  }

  public createCols() {
    this.cols = [
      { field: 'email', header: 'EMAIL' },
      { field: 'first_name', header: 'FIRST_NAME' },
      { field: 'last_name', header: 'LAST_NAME' },
      { field: 'id', header: 'ID' },
    ];
  }

  private getData() {
    this.httpApiService.getUsersPage().subscribe(
      (data) => {
        this.dataGrid = data.data;
        // this.showInfoMsg('Loading Complite')
        // this.showErrorMsg('Kernel Panik!!!!')
      },
      (error) => { console.log(error); }
    );
  }

  public reload() {
    this.getData();
  }

  closeCard(event) {
    this.showSimpleCard = false;
  }

  handleRowDblclick() {
    this.showSimpleCard = true;
  }

}
