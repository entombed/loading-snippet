import { Component, OnInit, HostListener, Output, EventEmitter, ElementRef, Renderer2, AfterViewInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { BaseCardComponent } from '@share/base-components/base-card/base-card.component';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-simple-card-second',
  templateUrl: './simple-card-second.component.html',
  styleUrls: ['./simple-card-second.component.less']
})
export class SimpleCardSecondComponent extends BaseCardComponent implements OnInit {

  cities = [
    { label: 'New York New York New York', value: 'NY' },
    { label: 'Rome', value: 'RM' },
    { label: 'London', value: 'LDN' },
    { label: 'IstanbulIstanbulIstanbulIstanbul 123London London ', value: 'IST' },
    { label: 'Paris', value: 'PRS' }
  ];
  val1 = 'Option 1';
  val2 = 'Option 22';
  date8 = new Date();
  selectedCategories: string[] = ['Technology', 'Sports'];
  selectedItem: any = null;
  currentIndex: any = null;
  displayDialogInfo = false;
  i18nWords = ['SAVED_MSG', 'CLOSE_MSG'];


  constructor(
    protected elementRef: ElementRef,
    protected renderer: Renderer2,
    protected messageService: MessageService,
    protected translateService: TranslateService
  ) {
    super(elementRef, renderer, messageService, translateService);
  }

  ngOnInit() {
    this.getTranslates(this.i18nWords, () => {
    });
  }

  public focusOut(event) {
    console.log(event)
  }

  public dropDown(event) {
    console.log(event)
  }
  public openTab(event) {
    console.log(event)
  }

}
