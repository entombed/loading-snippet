import { Component, OnInit, Renderer2, AfterContentInit, HostListener, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem } from 'primeng/api';
import { fromEvent } from 'rxjs';
import { LoginService } from '@share/services/login.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.less']
})
export class HomePageComponent implements OnInit, AfterContentInit {
  private display = true;
  private itemMenu: MenuItem[];
  // private source: any;
  private showBanner = true;

  constructor(
    private router: Router,
    private renderer: Renderer2,
    private elementRef: ElementRef,
    private loginService: LoginService
  ) {
    // this.source = fromEvent(document, 'scroll').subscribe(
    //   (data) => {
    //     console.log(data);
    //     console.log(window.pageYOffset);
    //   }
    // );
   }
  ngAfterContentInit() {

  }

  /**
   * отслеживаем скрол окна
   * прячем баннер если болше 400px проскроли вверх
   * показываем если скрол меньше 300px
   *
   * @author A.Bondarenko
   * @date 2020-01-14
   * @param {*} event
   * @memberof HomePageComponent
   */
  @HostListener('window:scroll', ['$event'])
  onWindowScroll(event) {
    // console.log(event.target.documentElement.scrollTop);
    const pageYOffset = event.target.documentElement.scrollTop;
    if (pageYOffset > 400) {
      this.showBanner = false;
    } else if (pageYOffset < 300) {
      this.showBanner = true;
    }
  }

  ngOnInit() {
    this.itemMenu = [{
      label: 'Grids',
      id: '1',
      items: [
        { id: '11', label: 'GridOperations01', command: (data) => { this.goto(data); }  },
        { id: '12', label: 'GridOperations02', command: (data) => { this.goto(data); }  },
      ]
    },
    {
      label: 'Edit',
      id: '2',
      items: [
        { id: '21', label: 'Undo', disabled: true },
        { id: '22', label: 'Redo', disabled: true }
      ]
    }];
  }

  public goto(data) {
    this.router.navigate([`/Operations/${data.item.label}`]);
  }

  /**
   * сворациваем все открытые акардио панель
   *
   * @author A.Bondarenko
   * @date 2020-01-14
   * @private
   * @memberof HomePageComponent
   */
  public collapseOpennedAccordions() {
    const listAccordion = this.elementRef.nativeElement.querySelectorAll('.ui-accordion-header.ui-state-active > a');
    listAccordion.forEach((element: HTMLElement) => {
      this.renderer.selectRootElement(element, true).click();
    });
  }

  /**
   * кнопака выхода из приложения
   *
   * @author A.Bondarenko
   * @date 2020-01-14
   * @param {*} event
   * @memberof HomePageComponent
   */
  public logout(event) {
    this.loginService.logout();
    this.router.navigate(['/']);
  }
}
