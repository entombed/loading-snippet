import { Component, OnInit, Renderer2 } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '@services/login.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.less']
})
export class LoginPageComponent implements OnInit {
  public showProgressBar = false;
  public userLogin: string = null;
  public userPassword: string = null;
  element: any;
  constructor(
    private router: Router,
    private loginService: LoginService,
    private renderer: Renderer2
  ) { }

  ngOnInit() {
  }

  public handleClick() {
    // this.showProgressBar = true;
    this.loginService.login();
    this.router.navigate(['/Operations']).then((data: boolean) => {
      if (!data) {
        this.userPassword = null;
      }
    });
  }

}
