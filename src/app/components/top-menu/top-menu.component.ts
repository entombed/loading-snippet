import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-top-menu',
  templateUrl: './top-menu.component.html',
  styleUrls: ['./top-menu.component.less']
})
export class TopMenuComponent implements OnInit {
  private menuItems: MenuItem[];

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
    this.menuItems = [
      {
        label: 'Reesters',
        // command: (data) => { this.goto('Reester'); }
        command: (data) => { this.router.navigate([`/Reester`]); }
      },
      { separator: true },
      {
        label: 'Operations',
        // command: (data) => { this.goto('Operations'); }
        command: (data) => { this.router.navigate([`/Operations`]); }
      },
      { separator: true },
      {
        label: 'Dictionry',
        command: (data) => { this.goto(data); },
        disabled: true
      }
    ];
  }

  public goto(data) {
    this.router.navigate([`/${data}`]);
  }

}
